/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 19:33:10
 * @modify date 2020-08-23 19:33:10
 * @desc [Product actions for redux]
 */

import { PRODUCT_REQUEST, PRODUCT_SUCCESS, PRODUCT_FAIL, DETAILS_PRODUCT_REQUEST, DETAILS_PRODUCT_SUCCESS, DETAILS_PRODUCT_FAIL, UPDATE_PRICE_PRODUCT_SUCCESS, UPDATE_PRICE_PRODUCT_FAIL,
  UPDATE_IMAGE_PRODUCT_SUCCESS, UPDATE_IMAGE_PRODUCT_FAIL} from '../constants/product';
import config from '../../config';
import axios from 'axios';

const getProducts = () => async (dispatch) => {
  try {
    dispatch({ type: PRODUCT_REQUEST }); //if there's no response our action are going to be this
    const { data } = await axios.get(config.getProducts);
    dispatch({ type: PRODUCT_SUCCESS, payload: data.message });//if the server do a response our action and state are going to be this
  }
  catch (error) {
    dispatch({ type: PRODUCT_FAIL, payload: error.message });
  }
}

const getDetailsProduct = (id) => async (dispatch) => {
    try {
        dispatch({ type: DETAILS_PRODUCT_REQUEST, payload: id });
        const { data } = await axios.get(config.getProduct + id)
        dispatch({type: DETAILS_PRODUCT_SUCCESS, payload: data.message})
    } catch (error) {
        dispatch({type: DETAILS_PRODUCT_FAIL, payload: error.message})
    }
}

const updateProductPrice = (product) => async (dispatch) => {
  try {
      dispatch({type: UPDATE_PRICE_PRODUCT_SUCCESS, payload: product})
  } catch (error) {
      dispatch({type: UPDATE_PRICE_PRODUCT_FAIL, payload: error.message})
  }
}

const updateProductImage = (product) => async (dispatch) => {
  try {
      dispatch({type: UPDATE_IMAGE_PRODUCT_SUCCESS, payload: product})
  } catch (error) {
      dispatch({type: UPDATE_IMAGE_PRODUCT_FAIL, payload: error.message})
  }
}

export { getProducts, getDetailsProduct, updateProductPrice, updateProductImage }