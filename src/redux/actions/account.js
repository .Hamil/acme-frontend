/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-25 14:44:59
 * @modify date 2020-08-25 14:44:59
 * @desc [This is the actions that supplies the info to our reducer on the redux store]
 */

import Axios from "axios";
import Cookie from 'js-cookie';
import { USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS, USER_LOGIN_FAIL, USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_REGISTER_FAIL, USER_LOGOUT } from "../constants/account";
import config from '../../config';

const login = (email, password) => async (dispatch) => {
  dispatch({ type: USER_LOGIN_REQUEST, payload: { email, password } });
  try {
    const { data } = await Axios.post(config.login, { email, password });
    dispatch({ type: USER_LOGIN_SUCCESS, payload: data });
    Cookie.set('user', JSON.stringify(data)); //setting the information of the user on the cookies (no the password btw) ;)
  } catch (error) {
    const {response: {status}} = error;
    if(status === 401){
      dispatch({ type: USER_LOGIN_FAIL, payload: config.messageUnauthorized }); //here we handle the exception with code to be more accurate
    }else{
      dispatch({ type: USER_LOGIN_FAIL, payload: error.message });
    }
  }
}

const register = (name, email, password) => async (dispatch) => {
  dispatch({ type: USER_REGISTER_REQUEST, payload: { name, email, password } });
  try {
    const { data: {message} } = await Axios.post(config.register, { name, email, password });
    dispatch({ type: USER_REGISTER_SUCCESS, payload: message });
    Cookie.set('user', JSON.stringify(message)); //setting the information of the user on the cookies (no the password btw) ;)
  } catch (error) {
    dispatch({ type: USER_REGISTER_FAIL, payload: error.message });
  }
}

const logout = () => (dispatch) => {
  Cookie.remove("user");
  dispatch({ type: USER_LOGOUT })
}

export { login, register, logout };