import axios from 'axios';
import config from '../../config';
import Cookie from "js-cookie";//importing the cookie library to use it on the cart view
import { ERROR_CART_ITEM, CART_ADD_ITEM, CART_REMOVE_ITEM, CART_CLEAR_ALL_SUCCESS, CART_CLEAR_ALL_ERROR } from '../constants/cart';

const addToCart = (product, quantity) => async (dispatch, getState) => {
try {
        // const { data: {message} } = await axios.get(config.getProduct + productID);
        dispatch({type: CART_ADD_ITEM, payload: {
            product: product._id,
            name: product.name,
            image: product.image,
            price: product.price,
            stock: product.stock,
            productt: product,
            quantity
        }
        })
        const { card: { cartItems } } = getState();
        Cookie.set("cartItems", JSON.stringify(cartItems));
    } catch (error) {
        dispatch({type: ERROR_CART_ITEM, payload: error.message})
    }
}

const removeFromCart = productId => (dispatch, getState) => {
    dispatch({ type: CART_REMOVE_ITEM, payload: productId });
    const { card: { cartItems } } = getState();
    Cookie.set("cartItems", JSON.stringify(cartItems));
  }

const clearCart = () => (dispatch) => {
    try {
        dispatch({ type: CART_CLEAR_ALL_SUCCESS, payload: [] });
        Cookie.remove("cartItems");
    } catch (error) {
        dispatch({ type: CART_CLEAR_ALL_ERROR, payload: error.message });
    }
}

export { addToCart, removeFromCart, clearCart };