import Axios from "axios";
import {
  ORDER_CREATE_REQUEST, ORDER_CREATE_SUCCESS, ORDER_CREATE_FAIL,
  ORDER_DETAILS_REQUEST, ORDER_DETAILS_SUCCESS, ORDER_DETAILS_FAIL, ORDER_PAY_REQUEST, ORDER_PAY_SUCCESS, ORDER_PAY_FAIL, MY_ORDER_LIST_REQUEST, MY_ORDER_LIST_SUCCESS, MY_ORDER_LIST_FAIL, ORDER_DELETE_REQUEST, ORDER_DELETE_SUCCESS, ORDER_DELETE_FAIL, ORDER_LIST_REQUEST, ORDER_LIST_SUCCESS, ORDER_LIST_FAIL
} from "../constants/order";
import config from '../../config';

const createOrder = (order) => async (dispatch, getState) => {
  try {
    dispatch({ type: ORDER_CREATE_REQUEST, payload: order });
    const { userLogin: { user } } = getState();
    const { data: { data: newOrder } } = await Axios.post(config.saveOrder, order, {
      headers: {
        Authorization: ' Bearer ' + user.token
      }
    });
    dispatch({ type: ORDER_CREATE_SUCCESS, payload: newOrder });
  } catch (error) {
    dispatch({ type: ORDER_CREATE_FAIL, payload: error.message });
  }
}

const listMyOrders = () => async (dispatch, getState) => {
  try {
    dispatch({ type: MY_ORDER_LIST_REQUEST });
    const { userLogin: { user } } = getState();
    const { data } = await Axios.get(config.getMyOrders, {
      headers:
        { Authorization: 'Bearer ' + user.token }
    });
    dispatch({ type: MY_ORDER_LIST_SUCCESS, payload: data })
  } catch (error) {
    dispatch({ type: MY_ORDER_LIST_FAIL, payload: error.message });
  }
}

const listOrders = () => async (dispatch, getState) => {

  try {
    dispatch({ type: ORDER_LIST_REQUEST });
    const { userLogin: { user } } = getState();
    const { data: {message} } = await Axios.get(config.getOrders, {
      headers:
        { Authorization: 'Bearer ' + user.token }
    });
    dispatch({ type: ORDER_LIST_SUCCESS, payload: message })
  } catch (error) {
    dispatch({ type: ORDER_LIST_FAIL, payload: error.message });
  }
}

const detailsOrder = (orderId) => async (dispatch, getState) => {
  try {
    dispatch({ type: ORDER_DETAILS_REQUEST, payload: orderId });
    const { userLogin: { user } } = getState();
    const { data: {message} } = await Axios.get(config.getOrderByID + orderId, {
      headers:
        { Authorization: 'Bearer ' + user.token }
    });
    dispatch({ type: ORDER_DETAILS_SUCCESS, payload: message })
  } catch (error) {
    dispatch({ type: ORDER_DETAILS_FAIL, payload: error.message });
  }
}

const deleteOrder = (orderId) => async (dispatch, getState) => {
  try {
    dispatch({ type: ORDER_DELETE_REQUEST, payload: orderId });
    const { userLogin: { user } } = getState();
    const { data: {message} } = await Axios.delete(config.deleteOrder + orderId, {
      headers:
        { Authorization: 'Bearer ' + user.token }
    });
    dispatch({ type: ORDER_DELETE_SUCCESS, payload: message })
  } catch (error) {
    dispatch({ type: ORDER_DELETE_FAIL, payload: error.message });
  }
}
export { createOrder, detailsOrder, listMyOrders, listOrders, deleteOrder };