/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-24 20:31:09
 * @modify date 2020-08-24 20:31:09
 * @desc [The file that has the action of card component]
 */

import { CART_ADD_ITEM, CART_REMOVE_ITEM, CART_CLEAR_ALL_SUCCESS, CART_CLEAR_ALL_ERROR } from "../constants/cart";

function cartReducer(state = { cartItems: [] }, action) {
  switch (action.type) {
    case CART_ADD_ITEM:
      const item = action.payload;
      const product = state.cartItems.find(x => x.product === item.product); //here we get the cardITems state and filter the object with the one that we need (payload)
      if (product) {
        return {
          cartItems:
            state.cartItems.map(x => x.product === product.product ? item : x)
        };
      }
      return { cartItems: [...state.cartItems, item] };
    case CART_REMOVE_ITEM:
      return { cartItems: state.cartItems.filter(x => x.product !== action.payload) }
    case CART_CLEAR_ALL_SUCCESS:
      return { cartItems: action.payload }
    case CART_CLEAR_ALL_ERROR:
      return { cartItems: action.payload }
    default:
      return state
  }
}

export { cartReducer }