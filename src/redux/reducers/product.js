/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 19:32:22
 * @modify date 2020-08-23 19:32:22
 * @desc [Product reducer for redux]
 */

import { PRODUCT_REQUEST, PRODUCT_SUCCESS, PRODUCT_FAIL, DETAILS_PRODUCT_REQUEST, DETAILS_PRODUCT_SUCCESS, DETAILS_PRODUCT_FAIL, UPDATE_PRICE_PRODUCT_REQUEST, UPDATE_PRICE_PRODUCT_SUCCESS, UPDATE_PRICE_PRODUCT_FAIL,
  UPDATE_IMAGE_PRODUCT_REQUEST, UPDATE_IMAGE_PRODUCT_SUCCESS, UPDATE_IMAGE_PRODUCT_FAIL } from '../constants/product';

function productReducer(state = { products: [] }, action) { // here we have the state empty because we return it in case that our product actions do not match

  switch (action.type) {
    case PRODUCT_REQUEST:
      return { loading: true };
    case PRODUCT_SUCCESS:
      return { loading: false, products: action.payload };
    case PRODUCT_FAIL:
      return { loading: false, error: action.payload }
    default:
      return state;
  }
}

function detailsProductReducer(state = { products: {} }, action) { // here we have the state empty because we return it in case that our product actions do not match

    switch (action.type) {
      case UPDATE_IMAGE_PRODUCT_SUCCESS:
        return { loading: false, product: action.payload };
      case UPDATE_IMAGE_PRODUCT_FAIL:
        return { loading: false, error: action.payload }
      case UPDATE_PRICE_PRODUCT_SUCCESS:
        return { loading: false, product: action.payload };
      case UPDATE_PRICE_PRODUCT_FAIL:
        return { loading: false, error: action.payload }
      case DETAILS_PRODUCT_REQUEST:
        return { loading: true };
      case DETAILS_PRODUCT_SUCCESS:
        return { loading: false, product: action.payload };
      case DETAILS_PRODUCT_FAIL:
        return { loading: false, error: action.payload }
      default:
        return state;
    }
  }

export { productReducer, detailsProductReducer }