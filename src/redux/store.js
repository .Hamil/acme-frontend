import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { productReducer, detailsProductReducer } from './reducers/product';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import { cartReducer } from './reducers/cart';
import { userLoginReducer, userRegisterReducer } from './reducers/account';
import {
    orderCreateReducer,
    orderDetailsReducer,
    myOrderListReducer,
    orderListReducer,
    orderDeleteReducer,
    orderPayReducer,
  } from './reducers/order';

const cartItems = Cookie.getJSON("cartItems") || [];
const user = Cookie.getJSON("user") || null;

const initialState = { cart: { cartItems }, userLogin: { user }};
const reducer = combineReducers({
    productState: productReducer,
    detailsProductState: detailsProductReducer,
    card: cartReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    myOrderList: myOrderListReducer,
    orderCreate: orderCreateReducer,
    orderDetails: orderDetailsReducer,
    orderList: orderListReducer,
    orderDelete: orderDeleteReducer,
    orderPay: orderPayReducer
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // attaching the redux devtools to the store
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk))); // making the structure of a redux store (dispach, reducer and state)

export default store;