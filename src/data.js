export default {
    products: [
      {
        id: '1',
        name: 'Slim Shirt',
        category: 'Shirts',
        image: '/images/d1.jpg',
        price: 60,
        brand: ' Nike',
        rating: 4.5,
        numReviews: 10,
        description: "Este producto es un t-shirt gris"
      },
      {
        id: '2',
        name: 'Fit Shirt',
        category: 'Shirts',
        image: '/images/d1.jpg',
        price: 50,
        brand: ' Nike',
        rating: 4.2,
        numReviews: 5,
        description: "Este producto es un t-shirt gris"
      },
      {
        id: '3',
        name: 'Best Pants',
        category: 'Pants',
        image: '/images/d1.jpg',
        price: 70,
        brand: ' Nike',
        rating: 4.5,
        numReviews: 8,
        description: "Este producto es un t-shirt gris"
      }, {
        id: '4',
        name: 'Best Pants',
        category: 'Pants',
        image: '/images/d1.jpg',
        price: 70,
        brand: ' Nike',
        rating: 4.5,
        numReviews: 8,
        description: "Este producto es un t-shirt gris"
      },
    ]
  }