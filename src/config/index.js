/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 17:12:18
 * @modify date 2020-08-23 17:12:18
 * @desc [description]
 */



export default {
    // ----------------- PRODUCT ENDPOINTS -----------------
    "getProducts": "/acme/get-products",
    "getProduct": "/acme/get-product/",
    // ----------------- ORDER ENDPOINTS -----------------
    "saveOrder": "/acme/order",
    "getOrders": "/acme/get-orders",
    "getOrderByID": "/acme/order/",
    "deleteOrder": "/acme/order/",
    "getMyOrders": "/acme/mine",
    // ----------------- AUTH ENDPOINTS -----------------
    "login": "/acme/auth/login",
    "logout": "/acme/auth/logout",
    "whoami": "/acme/auth/me",
    "register": "/acme/auth/register",
    // ----------------- MESSAGES -----------------
    "messageLoading": "Cargando...",
    "messageProblemsProduct": "Problemas intentando obtener este producto",
    "messageUnauthorized": "Credenciales erroneas"
}