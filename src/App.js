import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Product from './components/Product';
import Cart from './components/Cart'
import Login from './components/Login';
import PlaceOrder from './components/PlaceOrder';
import Orders from './components/Orders';
import Order from './components/Order';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from './redux/actions/account'
import Register from './components/Register';

function App() {

  //getting the information of the user
  const userLogin = useSelector(state => state.userLogin);
  const { user } = userLogin;
  const dispatch = useDispatch();

  const logOutHandler = () => {
    dispatch(logout());
  }
  
  return (
    <BrowserRouter>
      <div className="grid-container">
        <header className="header">
          <div className="brand">
            <Link to="/">ACME SRL</Link>
          </div>
          <div className="header-links">
          <Link to="/cart">Carrito</Link>
              {
                user? <div className="dropdown">
                <a href="#">{user.name}</a>
                <ul className="dropdown-content">
                  <li>
                    <Link to="/orders">Ordenes</Link>
                  </li>
                  <li>
                    <button onClick={logOutHandler}>Cerrar sesion</button>
                  </li>
                </ul>
              </div> : <Link to="/login">Iniciar sesion</Link>
              }
          </div>
        </header>
        <main className="main">
          <div className="content">
            <Route path="/register" component={Register} />
            <Route path="/order/:id" component={Order} />
            <Route path="/orders" component={Orders}/>
            <Route path="/placeorder" component={PlaceOrder}/>
            <Route path="/login" component={Login}/>
            <Route path="/cart/:id?" component={Cart}/>
            <Route path="/product/:id" component={Product} />
            <Route path="/" exact={true} component={Home} />
          </div>
        </main>
        <footer className="footer">ACME SRL</footer>
      </div>
     </BrowserRouter>
  );
}

export default App;