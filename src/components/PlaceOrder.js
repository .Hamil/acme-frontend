/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-25 23:42:50
 * @modify date 2020-08-25 23:42:50
 * @desc [This is the component that we use to display the order information]
 */

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { createOrder } from '../redux/actions/order';

function PlaceOrder(props) {

    const cart = useSelector(state => state.card);
    const orderCreate = useSelector(state => state.orderCreate);
    const { loading, success, error, order } = orderCreate;
  
    const { cartItems, shipping, payment } = cart;
    
    const itemsPrice = cartItems.reduce((a, c) => a + c.price * c.quantity, 0);
    const shippingPrice = itemsPrice > 100 ? 0 : 10;
    const taxPrice = 0.18 * itemsPrice;
    const totalPrice = itemsPrice + shippingPrice + taxPrice;
  
    const dispatch = useDispatch();

  const placeOrderHandler = () => {
    // create an order
    dispatch(createOrder({
      orderItems: cartItems, shipping, payment, itemsPrice, shippingPrice,
      taxPrice, totalPrice
    }));

    props.history.push("/orders");
  }


return loading ? <div>Procesando orden...</div> : error ? <div>Error procesando la orden: {error}</div> : <div>
  <div className="placeorder">
    <div className="placeorder-info">
      <div>
        <h3>
          Direccion
        </h3>
        <div>
          Av 27 de Febrero Esq 27 de Febrero, Santo Domingo,
        809, Dominican Republic,
        </div>
      </div>
      <div>
        <h3>Pago</h3>
        <div>
          Método de pago: Paypal
        </div>
      </div>
      <div>
        <ul className="cart-list-container">
          <li>
            <h3>
              Carrito
        </h3>
            <div>
              Precio
        </div>
          </li>
          {
            cartItems.length === 0 ?
              <div>
                El carrito está vacio
        </div>
              :
              cartItems.map(item =>
                <li>
                  <div className="cart-image">
                    <img src={item.image} alt="product" />
                  </div>
                  <div className="cart-name">
                    <div>
                      <Link to={"/product/" + item.product}>
                        {item.name}
                      </Link>

                    </div>
                    <div>
                      Cantidad: {item.quantity}
                    </div>
                  </div>
                  <div className="cart-price">
                    ${item.price}
                  </div>
                </li>
              )
          }
        </ul>
      </div>

    
    </div>
    <div className="placeorder-action">
      <ul>
        <li>
          <button className="button full-width" onClick={placeOrderHandler} >Generar Orden</button>
        </li>
        <li>
          <h3>Resumen</h3>
        </li>
        <li>
          <div>Articulos</div>
          <div>${Math.round(itemsPrice).toFixed(2)}</div>
        </li>
        <li>
          <div>Envio</div>
          <div>${Math.round(shippingPrice).toFixed(2)}</div>
        </li>
        <li>
          <div>Impuestos</div>
          <div>${Math.round(taxPrice, 2).toFixed(2)}</div>
        </li>
        <li>
          <div>Total</div>
          <div>${Math.round(totalPrice).toFixed(2)}</div>
        </li>
      </ul>



    </div>

  </div>
</div>

}

export default PlaceOrder;