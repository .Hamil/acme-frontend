import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { login } from '../redux/actions/account'
import config from '../config';

function Login(props){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const userLogin = useSelector(state => state.userLogin);
    const { loading, user, error } = userLogin;
    const dispatch = useDispatch();

    useEffect(() => {
        if (user) {
          props.history.push("/"); //redirecting the user to the home screen (if user is logged)
        }
        return () => {
          //
        };
      }, [user]);

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(login(email, password)); //sending this info to the store of redux
    }

    return <div className="form">
    <form onSubmit={handleSubmit} >
      <ul className="form-container">
        <li>
          <h2>Iniciar Sesion</h2>
        </li>
        <li>
    {loading && <div>{config.messageLoading}</div>}
          {error && <div>{error}</div>}
        </li>
        <li>
          <label htmlFor="email">
            Correo
          </label>
          <input type="email" name="email" id="email" onChange={(e) => setEmail(e.target.value)}>
          </input>
        </li>
        <li>
          <label htmlFor="password">Contraseña</label>
          <input type="password" id="password" name="password" onChange={(e) => setPassword(e.target.value)}>
          </input>
        </li>
        <li>
          <button type="submit" className="button">Entrar</button>
        </li>
        <li>
          No tengo cuenta, registrarme
        </li>
        <li>
          <Link to="/register" className="button secondary text-center" >Crear cuenta en ACME</Link>
        </li>
      </ul>
    </form>
  </div>
}

export default Login;