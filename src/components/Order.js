/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-26 11:28:17
 * @modify date 2020-08-26 11:28:17
 * @desc [This file allow us show an order by id]
 */

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsOrder } from '../redux/actions/order';
import config from '../config';

function Order(props) {

  const orderDetails = useSelector(state => state.orderDetails);
  const { loading, order, error } = orderDetails;
  const dispatch = useDispatch();

  useEffect(() => {
      dispatch(detailsOrder(props.match.params.id)); //requesting the details of the order when the component it's rendered the firs time (did mount)
    return () => {
    };
  }, []);

return loading ? <div>{config.messageLoading}</div> : error ? <div>{error}</div> :

    <div>
      <div className="placeorder">
        <div className="placeorder-info">
            <Link to="/orders">Volver atras</Link>
          <div>
            <h3>
              Direccion
          </h3>
            <div>
              Av 27 de Febrero Esq Nunez de Caceres, Santo Domingo,
          809, Republica Dominicana,
          </div>
            <div>
              {order.isDelivered ? "Delivered at " + order.deliveredAt : "Not Delivered."}
            </div>
          </div>
          <div>
            <h3>Pago</h3>
            <div>
              Metodo de Pago: Paypal
            </div>
            <div>
              {order.isPaid ? "Paid at " + order.paidAt : "Not Paid."}
            </div>
          </div>
          <div>
            <ul className="cart-list-container">
              <li>
                <h3>
                  Carrito
          </h3>
                <div>
                  Precio
          </div>
              </li>
              {
                order.orderItems.length === 0 ?
                  <div>
                    Carrito vacio
                  </div>
                  :
                  order.orderItems.map(item =>
                    <li key={item._id}>
                      <div className="cart-image">
                        <img src={item.image} alt="product" />
                      </div>
                      <div className="cart-name">
                        <div>
                          <Link to={"/product/" + item.product}>
                            {item.name}
                          </Link>

                        </div>
                        <div>
                          Cantidad: {item.quantity}
                        </div>
                      </div>
                      <div className="cart-price">
                        ${item.price}
                      </div>
                    </li>
                  )
              }
            </ul>
          </div>


        </div>
        <div className="placeorder-action">
          <ul>
            <li>
              <h3>Resumen</h3>
            </li>
            <li>
              <div>Articulos</div>
              <div>${order.itemsPrice}</div>
            </li>
            <li>
              <div>Envio</div>
              <div>${order.shippingPrice}</div>
            </li>
            <li>
              <div>Impuesto</div>
              <div>${order.taxPrice}</div>
            </li>
            <li>
              <div>Total</div>
              <div>${order.totalPrice}</div>
            </li>
          </ul>
        </div>

      </div>
    </div>

}

export default Order;