import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import { getDetailsProduct, updateProductPrice, updateProductImage } from '../redux/actions/product';
import { addToCart } from '../redux/actions/cart';
import config from '../config'
import mongoose from 'mongoose';
/**
 *
 *
 * @param {*} props
 * @returns the product component
 */
function Product(props) {
  let detailsProductState = useSelector(state => state.detailsProductState);
  const { loading, product, error} = detailsProductState;
  const [ quantity, setQuantity ] = useState(1);
  const [ size, setSize ] = useState(null);
  const [ color, setColor ] = useState(null);
  const [ style, setStyle] = useState(null);
  const dispatch = useDispatch();


  const handleAddProductToCard = () => {
    dispatch(addToCart(product, quantity));
    props.history.push("/cart/" + props.match.params.id + "?quantity=" + quantity);// showing user the card
  }

  const priceSizeVariationHandler = value => {
    product.variations.sizes.find(element => {
      if(element.size === value){
        product._id = mongoose.Types.ObjectId();
        product.price = element.price;
        dispatch(updateProductPrice(product));
      }
    })
  }

  const priceStyleVariationHandler = value => {
    product.variations.styles.find(element => {
      if(element.style === value){
        product._id = mongoose.Types.ObjectId();
        product.name = element.style;
        product.price = element.price;
        dispatch(updateProductPrice(product));
      }
    })
  }

  const imageStyleVariationHandler = value => {
    product.variations.styles.find(element => {
      if(element.style === value){
        product._id = mongoose.Types.ObjectId();
        product.image = element.image;
        dispatch(updateProductImage(product));
      }
    })
  }

  const imageColorVariationHandler = value => {
    product.variations.colors.find(element => {
      if(element.color === value){
        product._id = mongoose.Types.ObjectId();
        product.name = product.name +  ' ' + element.color;
        product.image = element.image;
        dispatch(updateProductImage(product));
      }
    })
  }

  useEffect(() =>{
    dispatch(getDetailsProduct(props.match.params.id));
  },[])

  return <div>
    <div className="back-to-result">
      <Link to="/">Inicio</Link>
    </div>
  {loading ? <div>{config.messageLoading}</div> 
    : 
      error ? <div>{error}</div>
    :
      product ? (
    <div className="details">
      <div className="details-image">
        <img src={product.image} alt="product" ></img>
      </div>
      <div className="details-info">
        <ul>
          <li>
            <h4>{product.name}</h4>
          </li>
          <li>
          Precio: <b>${product.price}</b>
          </li>
          <li>
            Descripcion:
            <div>
              {product.description}
            </div>
          </li>
        </ul>
      </div>
      <div className="details-action">
        <ul>
                <li>
                  Cantidad:{' '}
                  <select
                    value={quantity}
                    onChange={(e) => {
                      setQuantity(e.target.value);
                    }}
                  >
                    {[...Array(product.stock).keys()].map((x) => (
                      <option key={x + 1} value={x + 1}>
                        {x + 1}
                      </option>
                    ))}
                  </select>
                </li>
          {
            product.variations.styles.length === 0 ? null :
            <li>
              Estilo: <select value={style} onChange={(e) => { setStyle(e.target.value ); priceStyleVariationHandler(e.target.value); imageStyleVariationHandler(e.target.value); }}>
                {product.variations.styles.map(x => <option key={x} value={x.style}>{x.style}</option>)}
              </select>
            </li> 
          }
          {
            product.variations.colors.length === 0 ? null :  
            <li>
              Color: <select value={color} onChange={(e) => { setColor(e.target.value); imageColorVariationHandler(e.target.value); }}>
                {product.variations.colors.map(x => <option key={x} value={x.color}>{x.color}</option>)}
              </select>
            </li>
          }
          {
            product.variations.sizes.length === 0 ? null :
            <li>
              Tamaño: <select value={size} onChange={(e) => { setSize(e.target.value ); priceSizeVariationHandler(e.target.value); }}>
                {product.variations.sizes.map(x => <option key={x} value={x.size}>{x.size}</option>)}
              </select>
            </li>
          }
          
          <li>
            {product.stock > 0 ? <button onClick={handleAddProductToCard}>Agregar al carrito</button>
              : 
                <div>No disponible</div>
            }
          </li>
        </ul>
      </div>

    </div>
    ) : <div>{config.messageProblemsProduct}</div>
  }
  </div>
}
export default Product;