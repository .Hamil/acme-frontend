import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, removeFromCart } from '../redux/actions/cart';
import { Link } from 'react-router-dom';

function Cart(props){

    const productID = props.match.params.id;
    const quantity = props.location.search ? Number(props.location.search.split("=")[1]): 1;
    const dispatch = useDispatch();
    const cart = useSelector(state => state.card);
    const { cartItems } = cart;
    const userLogin = useSelector(state => state.userLogin);
    const { user } = userLogin;
    
    const handleRemoveFromCart = productID => {
        dispatch(removeFromCart(productID));
    }

    const handleCheckout = () => {
        props.history.push("/placeorder")
    }

    useEffect(() => {
        if(productID){
            // dispatch(addToCart(productID, quantity))//sending the productID to our add action on cart 
        }
    }, []);
    
    useEffect(() => {
      if(cartItems){
        // dispatch(addToCart(productID, quantity))//sending the productID to our add action on cart 
      }
    }, [cartItems]);
    return <div className="cart">
    <div className="cart-list">
      <ul className="cart-list-container">
        <li>
          <h3>
            Carrito
          </h3>
          <div>
            Precio
          </div>
        </li>
        {
          cartItems.length === 0 ?
            <div>
              El carrito esta vacio
          </div>
            :
            cartItems.map(item =>
              <li>
                <div className="cart-image">
                  <img src={item.image} alt="product" />
                </div>
                <div className="cart-name">
                  <div>
                    <Link to={"/product/" + item.product}>
                      {item.name}
                    </Link>

                  </div>
                  <div>
                    Cantidad:
                  <select value={item.quantity} onChange={(e) => dispatch(addToCart(item.productt, e.target.value))}>
                      {[...Array(item.stock).keys()].map(x =>
                        <option key={x + 1} value={x + 1}>{x + 1}</option>
                      )}
                    </select>
                    <button type="button" className="button" onClick={() => handleRemoveFromCart(item.product)} >
                      Borrar
                    </button>
                  </div>
                </div>
                <div className="cart-price">
                  ${item.price}
                </div>
              </li>
            )
        }
      </ul>

    </div>
    <div className="cart-action">
      <h3>
          {/* adding the number function because the values are string */}
        Subtotal ( {cartItems.reduce((a, c) => Number(a) + Number(c.quantity), 0)} items)
        :
         $ {cartItems.reduce((a, c) => a + Number(c.price) * Number(c.quantity), 0)}
      </h3>
      {user ? <button onClick={handleCheckout} className="button full-width" disabled={cartItems.length === 0}>
        Ir a caja
      </button> : 
        <Link to="/login"><button className="button full-width">Iniciar sesion</button></Link>
      }
      

    </div>

  </div>
}

export default Cart