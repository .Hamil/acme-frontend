import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { listOrders, deleteOrder
 } from '../redux/actions/order';
import { clearCart } from '../redux/actions/cart';

function Orders(props) {
  const orderList = useSelector(state => state.orderList);
  const { loading, orders, error } = orderList;

  const orderDelete = useSelector(state => state.orderDelete);
  const { loading: loadingDelete, success: successDelete, error: errorDelete } = orderDelete;

  //getting the information of the user
  const userLogin = useSelector(state => state.userLogin);
  const { user } = userLogin;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(clearCart());
  }, [orders])

  useEffect(() => {
    dispatch(listOrders());
    return () => {
      //
    };
  }, [successDelete]);

  const deleteHandler = (order) => {
    dispatch(deleteOrder(order._id));
  }
  return loading ? <div>Cargando...</div> :
    <div className="content content-margined">

      <div className="order-header">
        <h3>Ordenes</h3>
      </div>
      <div className="order-list">

        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>FECHA</th>
              <th>USUARIO</th>
              <th>TOTAL</th>
              <th>ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              order.user.username === user.email ? //getting the orders just for the user that is logged
              <tr key={order._id}>
              <td>{order._id}</td>
              <td>{order.createdAt}</td>
              <td>{order.user.user}</td>
              <td>${order.totalPrice}</td>
              <td>
                <Link to={"/order/" + order._id} className="button secondary" >Detalles</Link>
                {' '}
                <button type="button" onClick={() => deleteHandler(order)} className="button secondary">Borrar</button>
              </td>
            </tr> : null
              ))
            }
          </tbody>
        </table>

      </div>
    </div>
}
export default Orders;